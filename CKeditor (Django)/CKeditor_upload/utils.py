from __future__ import absolute_import

import os.path
import random
import re
import string

from django.conf import settings
from django.template.defaultfilters import slugify
from django.utils.encoding import force_text
from django.utils.module_loading import import_string

fileicons_path = '{0}/file-icons/'.format(getattr(settings, 'CKEDITOR_FILEICONS_PATH', '/static/ckeditor'))
override_icons = getattr(settings, 'CKEDITOR_FILEICONS', [])
ckeditor_icons = [
    (r'\.pdf$', fileicons_path + 'pdf.png'),
    (r'\.doc$|\.docx$|\.odt$', fileicons_path + 'doc.png'),
    (r'\.txt$', fileicons_path + 'txt.png'),
    (r'\.ppt$', fileicons_path + 'ppt.png'),
    (r'\.xls$', fileicons_path + 'xls.png'),
    ('.*', fileicons_path + 'file.png'), 
]
CKEDITOR_FILEICONS = override_icons + ckeditor_icons

IMAGE_EXTENSIONS = {'.jpg', '.jpeg', '.png', '.gif'}

def get_storage_class():
    return import_string(getattr(settings, 'CKEDITOR_STORAGE_BACKEND', 'django.core.files.storage.DefaultStorage'))()


storage = get_storage_class()


def slugify_filename(filename):
    name, ext = os.path.splitext(filename)
    slugified = get_slugified_name(name)
    return slugified + ext


def get_slugified_name(filename):
    slugified = slugify(filename)
    return slugified or get_random_string()


def get_random_string():
    return ''.join(random.sample(string.ascii_lowercase * 6, 6))


def get_icon_filename(file_name):
    for regex, iconpath in CKEDITOR_FILEICONS:
        if re.search(regex, file_name, re.I):
            return iconpath


def get_thumb_filename(file_name):
    return force_text('{0}_thumb{1}').format(*os.path.splitext(file_name))


def get_media_url(path):
    return storage.url(path)


def is_valid_image_extension(file_path):
    extension = os.path.splitext(file_path.lower())[1]
    return extension in IMAGE_EXTENSIONS
