import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SiteRoutingModule } from './site-routing.module';
import { SiteComponent} from './site.component';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { StaticModule, SliderModule } from 'src/app/app.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    SiteComponent
  ],
  providers:[
    SiteComponent
  ],
  imports: [
    SiteRoutingModule,
    NgxPageScrollModule,
    CommonModule,
    NgxPageScrollModule, 
    
    StaticModule,
    SliderModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
  ]
})
export class SiteModule { }
