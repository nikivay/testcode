import { Component, OnInit, PipeTransform, Pipe } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { plainToClass } from 'class-transformer';
import { DomSanitizer } from '@angular/platform-browser';
import { Observable, of, forkJoin } from 'rxjs';
import { map, shareReplay, catchError } from 'rxjs/operators';

import { Article, Event } from '../../../../classes/events';
import { Videos } from '../../../../classes/video';
import { Speaker, Book } from '../../../../classes/users';

import { ApiService } from '../../../../api/api.service';
import { VideoService } from '../../../../api/services/video.service';
import { PageDisplay, Tag } from 'src/app/classes/display';
import { Product } from 'src/app/classes/core';
import { Gallery, GalleryRef } from '@ngx-gallery/core';


@Component({
  selector: 'app-site',
  templateUrl: './site.component.html',
  styleUrls: ['./site.component.scss']
})
export class SiteComponent implements OnInit {
  constructor(
    private api:ApiService,
    private route:ActivatedRoute,    
    private gallery: Gallery,
    private _video:VideoService,
    private router:Router,
  ) { } 
  
  site_id: string;
  speakers: Array<Speaker>
  description:string
  videos: Array<Videos> = []
  schema=null
  articles:Array<Article> = []

  event_id:string;

  events$:Observable<Event>;
  site_diaplay$: Observable<PageDisplay>;

  books$:Observable<Book>[];
  product$: Observable<Product>[];
  speaker$: Observable<Speaker>[];
  articles$: Observable<Article>[];
  tags$: Observable<Tag>[];
  min_price: number;

  ngOnInit() {    
    let site_id = this.route.snapshot.paramMap.get("site_id")
    this.site_id = site_id
    if (site_id == null || site_id == ''){
      this.router.navigateByUrl('/talk')
    }
    let galleryRef: GalleryRef = this.gallery.ref('SiteSlider')

    this.site_diaplay$ = this.api.get('core/site/display/'+site_id+'/').pipe(
      catchError(val => {
        console.log(val.statusText)
        switch (val.statusText) {
          case "Not Found":
            this.api.open_snack('Страницы не существует.',null,"info")
            break;
        }
        this.router.navigateByUrl('/talk')
        return val
      }),
      map(site => plainToClass(PageDisplay,site)),
      map(site => {
        galleryRef.reset()
        site.Slider.forEach(slider =>{
          switch (slider.method) {
            case "image":
              galleryRef.addImage({
                src: slider.data,
              })
              break;
            case "Iframe":
              galleryRef.addIframe({
                src: slider.data
              })
              break;
            case "YouTube":
              galleryRef.addYoutube({
                src: slider.data
              })
              break;
          }
        })
        return site
      }),
      shareReplay(1),
    );
    
    this.site_diaplay$.subscribe(display => {
      this.articles$ = display.Articles.map(index => {
        return <Observable<Article>>this.api.get('core/article/'+index+'/').pipe(
          map(article => plainToClass(Article,article)),
          shareReplay(1),
        )
      })

      this.books$ = display.Books.map(book => {
        return <Observable<Book>>this.api.get('core/book/'+book+'/',{}).pipe(
          map(book => plainToClass(Book, book)),
          shareReplay(1),
        )
      })

      this.tags$ = display.Tags.map(tag => {
        return <Observable<Tag>>this.api.get('core/tag/'+tag+'/').pipe(
          map(tag => plainToClass(Tag, tag)),
          shareReplay(1)
        )
      })

      this.product$ = display.Products.map(index => {
        return <Observable<Product>>this.api.get('core/product/'+index+'/').pipe(
          map(product => plainToClass(Product, product)),
          shareReplay(1),
        )
      })

      this.speaker$ = display.Speakers.map(index =>{
        return  <Observable<Speaker>>this.api.get('core/speaker/id__'+index+'/').pipe(
          map(speaker => plainToClass(Speaker, speaker)),          
          shareReplay(1)
        )
      })

      
      forkJoin(this.product$).subscribe(products => {
        this.min_price = Math.min.apply(Math, products.map(product => product.Price))
      })
      
    })
  }

  sum(accumulator, currentValue){
    return accumulator+currentValue.Price
  }

  open_video(id:string ){
    this._video.open_video(id, ()=>{})
  }
}

@Pipe({
  name: 'safeHtml',
})
export class SafeHtmlPipe implements PipeTransform {

  constructor(
    private sanitizer:DomSanitizer
    ){}

  transform(html:string) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

}
